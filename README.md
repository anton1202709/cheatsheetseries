# Шпаргалка по безопасности Docker

## Введение

Docker - самая популярная технология контейнеризации. При правильном использовании она может повысить уровень безопасности (по сравнению с запуском приложений непосредственно на хосте). С другой стороны, некоторые неправильные настройки могут привести к снижению уровня безопасности или даже к появлению новых уязвимостей.

Цель этой шпаргалки - предоставить простой в использовании список распространенных ошибок безопасности и хороших практик, которые помогут вам защитить ваши контейнеры Docker.

## Правила

### ПРАВИЛО \#0 - Держите хост и Docker в актуальном состоянии.

Для предотвращения известных уязвимостей, связанных с выходом из контейнеров, которые обычно заканчиваются повышением привилегий до уровня root/администратора, очень важно обновлять Docker Engine и Docker Machine.

Кроме того, контейнеры (в отличие от виртуальных машин) разделяют ядро с хостом, поэтому эксплойты ядра, выполняемые внутри контейнера, будут напрямую поражать ядро хоста. Например, эксплойт повышения привилегий ядра ([как Dirty COW](https://github.com/scumjr/dirtycow-vdso)), выполненный внутри хорошо изолированного контейнера, приведет к получению root-доступа на хосте.

### ПРАВИЛО \#1 - Не открывайте сокет демона Docker (даже для контейнеров).

Docker socket */var/run/docker.sock* - это UNIX сокет, который прослушивает Docker. Это основная точка входа в API Docker. Владельцем этого сокета является root. Предоставление кому-либо доступа к нему равносильно предоставлению неограниченного доступа root к вашему хосту.

**Не включайте *tcp* сокет Docker daemon.** Если вы запускаете docker daemon с `-H tcp://0.0.0.0:XXX` или подобным, вы открываете незашифрованный и неаутентифицированный прямой доступ к Docker daemon, если хост подключен к интернету, это означает, что docker daemon на вашем компьютере может быть использован кем угодно из публичного интернета.
Если вы действительно, **действительно** должны это сделать, вам следует защитить это. Проверьте, как это сделать [следуя официальной документации Docker](https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-socket-option).

**Не открывайте */var/run/docker.sock* для других контейнеров**. Если вы запускаете свой образ docker с `-v /var/run/docker.sock://var/run/docker.sock` или подобным, вам следует изменить это. Помните, что монтирование сокета только для чтения не является решением проблемы, а только усложняет ее эксплуатацию. Эквивалент в файле docker-compose выглядит следующим образом:

```yaml
volumes:
- "/var/run/docker.sock:/var/run/docker.sock"
```

### ПРАВИЛО \#2 - Установить пользователя

Настройка контейнера на использование непривилегированного пользователя является лучшим способом предотвращения атак повышения привилегий. Этого можно добиться тремя различными способами, описанными ниже:

1. Во время выполнения с помощью опции `-u` команды `docker run`, например:

    ```bash
    docker run -u 4000 alpine
    ```

2. Во время сборки. Просто добавьте пользователя в Dockerfile и используйте его. Например:

    ```docker
    FROM alpine
    RUN groupadd -r myuser && useradd -r -g myuser myuser
    <ЗДЕСЬ СДЕЛАЙТЕ ТО, ЧТО ВЫ ДОЛЖНЫ ДЕЛАТЬ КАК ПОЛЬЗОВАТЕЛЬ ROOT, НАПРИМЕР, УСТАНОВКУ ПАКЕТОВ И Т.Д.>
    USER myuser
    ```

3. Включите поддержку пространства имен пользователей (`--userns-remap=default`) в [Docker daemon](https://docs.docker.com/engine/security/userns-remap/#enable-userns-remap-on-the-daemon).

Дополнительную информацию по этой теме можно найти в [Официальной документации Docker](https://docs.docker.com/engine/security/userns-remap/).

В kubernetes это можно настроить в [Security Context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) с помощью поля `runAsNonRoot`, например:

```yaml
kind: ...
apiVersion: ...
metadata:
  name: ...
spec:
  ...
  containers:
  - name: ...
    image: ....
    securityContext:
          ...
          runAsNonRoot: true
          ...
```

Как администратор кластера Kubernetes, вы можете настроить его с помощью [Pod Security Policies](https://kubernetes.io/docs/concepts/policy/pod-security-policy/).

### ПРАВИЛО \#3 - Ограничение возможностей (Предоставление только определенных возможностей, необходимых контейнеру)

[Возможности ядра Linux](http://man7.org/linux/man-pages/man7/capabilities.7.html) - это набор привилегий, которые могут быть использованы привилегированным лицом. Docker, по умолчанию, запускается только с некоторым набором возможностей.
Вы можете изменить это и отказаться от некоторых возможностей (используя `--cap-drop`), чтобы усилить ваши контейнеры docker, или добавить некоторые возможности (используя `--cap-add`), если это необходимо.
Помните, что не следует запускать контейнеры с флагом `--privileged` - это добавит контейнеру ВСЕ возможности ядра Linux.

Наиболее безопасной установкой является отказ от всех возможностей `--cap-drop all`, а затем добавление только необходимых. Например:

```bash
docker run --cap-drop all --cap-add CHOWN alpine
```

**И помните: Не запускайте контейнеры с флагом *--privileged*!!!**.

В kubernetes это можно настроить в [Security Context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) с помощью поля `capabilities`, например:

```yaml
kind: ...
apiVersion: ...
metadata:
  name: ...
spec:
  ...
  containers:
  - name: ...
    image: ....
    securityContext:
          ...
          capabilities:
            drop:
              - all
            add:
              - CHOWN
          ...
```

Как администратор кластера Kubernetes, вы можете настроить его с помощью [Pod Security Policies](https://kubernetes.io/docs/concepts/policy/pod-security-policy/).


### ПРАВИЛО \#4 - Добавить флаг -no-new-privileges

Всегда запускайте ваши образы docker с `--security-opt=no-new-privileges`, чтобы предотвратить повышение привилегий с помощью бинарных файлов `setuid` или `setgid`.

В kubernetes это можно настроить в [Security Context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) с помощью поля `allowPrivilegeEscalation`, например:

```yaml
kind: ...
apiVersion: ...
metadata:
  name: ...
spec:
  ...
  containers:
  - name: ...
    image: ....
    securityContext:
          ...
          allowPrivilegeEscalation: false
          ...
```

Как администратор кластера Kubernetes, вы можете обратиться к документации Kubernetes для его настройки с помощью [Pod Security Policies](https://kubernetes.io/docs/concepts/policy/pod-security-policy/).

### ПРАВИЛО \#5 - Отключить межконтейнерное взаимодействие (--icc=false)

По умолчанию межконтейнерная связь (icc) включена - это означает, что все контейнеры могут общаться друг с другом (используя [`docker0` bridged network](https://docs.docker.com/v17.09/engine/userguide/networking/default_network/container-communication/#communication-between-containers)).
Это можно отключить, запустив демон docker с флагом `--icc=false`.
Если icc отключен (icc=false), необходимо указать, какие контейнеры могут взаимодействовать, используя опцию --link=CONTAINER_NAME_or_ID:ALIAS.
См. подробнее в [Документация Docker - связь между контейнерами](https://docs.docker.com/v17.09/engine/userguide/networking/default_network/container-communication/#communication-between-containers).

В Kubernetes для этого можно использовать [Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/).

### ПРАВИЛО \#6 - Используйте модуль безопасности Linux (seccomp, AppArmor или SELinux)

**Прежде всего, не отключайте профиль безопасности по умолчанию!**

Рассмотрите возможность использования профиля безопасности типа [seccomp](https://docs.docker.com/engine/security/seccomp/) или [AppArmor](https://docs.docker.com/engine/security/apparmor/).

Инструкции, как это сделать внутри Kubernetes, можно найти в [Security Context documentation](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) и в [Kubernetes API documentation](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.18/#securitycontext-v1-core)

### ПРАВИЛО \#7 - Ограничьте ресурсы (память, CPU, дескрипторы файлов, процессы, перезагрузки).

Лучший способ избежать DoS-атак - ограничение ресурсов. Вы можете ограничить [память](https://docs.docker.com/config/containers/resource_constraints/#memory), [CPU](https://docs.docker.com/config/containers/resource_constraints/#cpu), максимальное количество перезапусков (`--restart=on-failure:<number_of_restarts>`), максимальное количество файловых дескрипторов (`--ulimit nofile=<number>`) и максимальное количество процессов (`--ulimit nproc=<number>`).

[Более подробную информацию об ulimits смотрите в документации] (https://docs.docker.com/engine/reference/commandline/run/#set-ulimits-in-container---ulimit).

Вы также можете сделать это внутри Kubernetes: [Assign Memory Resources to Containers and Pods](https://kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource/), [Assign CPU Resources to Containers and Pods](https://kubernetes.io/docs/tasks/configure-pod-container/assign-cpu-resource/) и [Assign Extended Resources to a Container
](https://kubernetes.io/docs/tasks/configure-pod-container/extended-resource/)

### ПРАВИЛО \#8 - Установите файловую систему и тома только для чтения

**Запускайте контейнеры с файловой системой только для чтения**, используя флаг `--read-only`. Например:

```bash
docker run --read-only alpine sh -c 'echo "whatever" > /tmp'
```

Если приложению внутри контейнера нужно что-то временно сохранить, сочетайте флаг `--read-only` с `--tmpfs` следующим образом:

```bash
docker run --read-only --tmpfs /tmp alpine sh -c 'echo "whatever" > /tmp/file'
```

Эквивалент в файле docker-compose будет таким:

```yaml
version: "3"
services:
  alpine:
    image: alpine
    read_only: true
```

Эквивалент в kubernetes в [Security Context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) будет:

```yaml
kind: ...
apiVersion: ...
metadata:
  name: ...
spec:
  ...
  containers:
  - name: ...
    image: ....
    securityContext:
          ...
          readOnlyRootFilesystem: true
          ...
```

Кроме того, если том смонтирован только для чтения, **монтируйте его как доступный только для чтения**.
Это можно сделать, добавив `:ro` к `-v` следующим образом:

```bash
docker run -v volume-name:/path/in/container:ro alpine
```

Или с помощью опции `--mount`:

```bash
docker run --mount source=volume-name,destination=/path/in/container,readonly alpine
```


### ПРАВИЛО \#9 - Используйте инструменты статического анализа

Для обнаружения контейнеров с известными уязвимостями - сканируйте изображения с помощью инструментов статического анализа.

- Бесплатно
    - [Clair](https://github.com/coreos/clair)
    - [ThreatMapper](https://github.com/deepfence/ThreatMapper)
    - [Trivy](https://github.com/knqyf263/trivy)
- Коммерческий
    - [Snyk](https://snyk.io/) **(доступен открытый и бесплатный вариант)**
    - [anchore](https://anchore.com/opensource/) **(доступен открытый исходный код и бесплатный вариант)**
    - [JFrog XRay](https://jfrog.com/xray/)
    - [Qualys](https://www.qualys.com/apps/container-security/)

Для обнаружения секретов в изображениях:

- [ggshield](https://github.com/GitGuardian/ggshield) **(доступен бесплатный вариант с открытым исходным кодом)**.
- [SecretScanner](https://github.com/deepfence/SecretScanner) **(с открытым исходным кодом)**.

Для обнаружения неправильных конфигураций в Kubernetes:

- [kubeaudit](https://github.com/Shopify/kubeaudit)
- [kubesec.io](https://kubesec.io/)
- [kube-bench](https://github.com/aquasecurity/kube-bench)

Для обнаружения неправильных конфигураций в Docker:

- [inspec.io](https://www.inspec.io/docs/reference/resources/docker/)
- [dev-sec.io](https://dev-sec.io/baselines/docker/)
- [Docker Bench for Security](https://github.com/docker/docker-bench-security)

### ПРАВИЛО \#10 - Установите уровень протоколирования не ниже INFO

По умолчанию демон Docker настроен на базовый уровень логирования 'info', если это не так: установите уровень логирования демона Docker на 'info'. Обоснование: Установка соответствующего уровня журнала настраивает демон Docker на регистрацию событий, которые вы захотите просмотреть позже. Базовый уровень журнала 'info' и выше будет фиксировать все журналы, кроме журналов отладки. Пока нет необходимости, не следует запускать демон docker на уровне журнала 'debug'.

Чтобы настроить уровень журнала в docker-compose:

```bash
docker-compose --log-level info up
```

### Правило \#11 - Проверяйте Dockerfile во время сборки


Многие проблемы можно предотвратить, следуя некоторым лучшим практикам при написании Dockerfile. Добавление линтера безопасности в качестве шага в конвейер сборки может помочь избежать дальнейших головных болей. Вот некоторые моменты, которые стоит проверить:

- Убедитесь, что указана директива `USER`.
- Убедитесь, что версия базового образа прикреплена к базе
- Убедитесь, что версии пакетов ОС закреплены.
- Избегайте использования `ADD` в пользу `COPY`.
- Избегайте использования curl bashing в директивах `RUN`.

Ссылки:

- [Docker Baselines on DevSec](https://dev-sec.io/baselines/docker/)
- [Использование командной строки Docker](https://docs.docker.com/engine/reference/commandline/cli/)
- [Обзор docker-compose CLI](https://docs.docker.com/compose/reference/overview/)
- [Настройка драйверов ведения журналов](https://docs.docker.com/config/containers/logging/configure/)
- [Просмотр журналов для контейнера или службы](https://docs.docker.com/config/containers/logging/)
- [Лучшие практики безопасности Dockerfile](https://cloudberry.engineering/article/dockerfile-security-best-practices/)

### Правило \#12 - Запускайте Docker в режиме root-less

Режим Rootless обеспечивает запуск демона Docker и контейнеров от имени непривилегированного пользователя, что означает, что даже если злоумышленник вырвется из контейнера, он не будет обладать привилегиями root на хосте, что в свою очередь существенно ограничивает площадь атаки.

Режим Rootless стал экспериментальным в Docker Engine v20.10 и должен быть рассмотрен для обеспечения дополнительной безопасности, если [известные ограничения](https://docs.docker.com/engine/security/rootless/#known-limitations) не являются препятствием.

> Режим Rootless позволяет запускать демон Docker и контейнеры от имени пользователя, не являющегося пользователем root, чтобы уменьшить потенциальные уязвимости в демоне и среде выполнения контейнеров.
> Режим Rootless не требует привилегий root даже во время установки демона Docker, если выполнены [предварительные условия](https://docs.docker.com/engine/security/rootless/#prerequisites).
> Бескорневой режим был представлен в Docker Engine v19.03 в качестве экспериментальной функции. В Docker Engine v20.10 режим Rootless стал экспериментальным.

Подробнее о режиме rootless, его ограничениях, инструкциях по установке и использованию читайте на странице [Документация Docker](https://docs.docker.com/engine/security/rootless/).